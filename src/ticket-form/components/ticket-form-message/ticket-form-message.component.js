import "./styles.scss";
import _ from "main/app.mixins";
import template from "./ticket-form-message.html";
import {Component} from "main/decorators";

import closeIcon from "images/icons/i-close.svg";
import attachmentIcon from "images/icons/i-clip-thin.svg";

@Component({
	template,
	selector: "ticketFormMessage",
	bindings: {
		ticket: "<",
		filesToUpload: "<",
		onChange: "<",
		placeholder: "<"
	}
})
export class TicketFormMessageComponent {
	/**
	 * @ngInject
	 */
	constructor($scope, $element, ValidationMessagesService, ImagePreview) {
		this.services = {$scope, $element, ValidationMessagesService, ImagePreview};
		this.icons = {closeIcon, attachmentIcon};

		this.imagePreview = ImagePreview;

		this.questionHintVisible = true;
	}

	changeAttachments(items) {
		this.filesToUpload = items;
	}

	onFocusQuestion() {
		this.questionHintVisible = false;
		document.querySelector(".question-wrapper textarea").focus();
	}

	onBlurQuestion() {
		this.questionHintVisible = !this.ticket.description;
	}

	removePreview(index) {
		this.filesToUpload.splice(index, 1);
	}
}
