import template from "./ticket-form-data-processing-agreement.html";
import {Component} from "main/decorators";
import "./styles.scss";

@Component({
	template,
	selector: "ticketFormDataProcessingAgreement",
	bindings: {
		onChange: "&"
	}
})
export class TicketFormDataProcessingAgreementComponent {
	/**
	 * @ngInject
	 */
	constructor($scope, $uibModal) {
		this.services = {$scope, $uibModal};
		this.isAgree = false;
		this.contactAgree = false;
	}

	changeFlag(field) {
		this[field] = !this[field];
		this.onChange({
			agreementData: {
				isAgree: this.isAgree,
				contactAgree: this.contactAgree
			}
		});
	}

	showAgreementForm(){
		const {$scope, $uibModal} = this.services;
		$uibModal.open({
			animation: true,
			component: "ticketFormAgreementForm",
			size: "md",
			windowClass: "ticket-agreement-text-modal"
		});
		//$scope.$emit("support-form:view-mode-changed", "agreement");
	}
}