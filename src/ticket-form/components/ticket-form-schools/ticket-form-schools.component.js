import _ from "main/app.mixins";
import template from "./ticket-form-schools.html";
import {Component} from "main/decorators";

@Component({
	template,
	selector: "ticketFormSchools",
	bindings: {
		onChange: "&"
	}
})
export class TicketFormSchoolsComponent {
	/**
	 * @ngInject
	 */
	constructor(Schools) {
		this.services = {Schools};
		this.schools = [];
		this.selected = null;
		this.promise = null;
		this.selectParams = {
			getLabel: (option) => option.short_title,
			getKey: (option) => option.id
		};
	}

	$onInit() {
		this.$loadSchools();
	}

	/**
	 * Загрузка списка школ
	 *
	 * @returns {*}
	 */
	$loadSchools() {
		const {Schools} = this.services;

		this.promise = Schools
			.getAll()
			.then((schools) => {
				this.schools = schools;
			});
	}

	/**
	 * Колбэк на выбор школы
	 */
	onSelectSchool(model) {
		const organizationId = _.get(model, "id", null);

		this.selected = model;
		this.onChange({organizationId});
	}
}