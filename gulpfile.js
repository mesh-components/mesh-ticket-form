const npmi = require("npmi");
const gulp = require("gulp");
const replace = require("gulp-replace");
const packageJson = require("./package.json");

// починяет loader-utils
gulp.task("fix-loader-utils", function () {
	if (/^2.+/.test(npmi.NPM_VERSION)) {
		correct("./node_modules/css-loader/node_modules/loader-utils");
		correct("./node_modules/html-loader/node_modules/loader-utils");
	}

	if (/^3.+/.test(npmi.NPM_VERSION)) {
		correct("./node_modules/loader-utils");
	}
});

// прописывает текущую ревизию в ./build/index.html
gulp.task("set-revision", function () {
	gulp.src("./build/index.html")
		.pipe(replace("set-revision", "v" + packageJson.version))
		.pipe(gulp.dest("./build/"));
});


function correct(path) {
	gulp
		.src(path + "/index.js")
		.pipe(replace(/request = "\.\/" \+ url;/, "request = url;"))
		.pipe(gulp.dest(path + "/"));
}