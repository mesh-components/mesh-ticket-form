import "./styles.scss";
import _ from "main/app.mixins";
import template from "./ticket-successeful-dialog.template.html";
import {Component} from "main/decorators";

import checkIcon from "images/icons/i-modal-circle-ok.svg";

@Component({
	template,
	selector: "ticketSuccessfulDialog",
	bindings: {
		resolve: "<",
		close: "&"
	}
})
export class TicketSuccessfulDialogComponent {
	/**
	 * @ngInject
	 */
	constructor() {
		this.icons = {checkIcon};
	}
}