const fs = require("fs");

const packageJson = JSON.parse(fs.readFileSync("./package.json", "utf8"));

const gitPackages = Object.keys(packageJson.dependencies).filter((name) => {
	return packageJson.dependencies[name].includes("git+");
});

console.log("Clear git dependencies...");

gitPackages.forEach((pckg) => {
	deleteFolderRecursive("./node_modules/" + pckg);
});

/**
 *
 * */
function deleteFolderRecursive(path) {
	if (fs.existsSync(path)) {
		fs.readdirSync(path).forEach((file, index) => {
			const curPath = path + "/" + file;
			if (fs.lstatSync(curPath).isDirectory()) { // recurse
				deleteFolderRecursive(curPath);
			} else { // delete file
				fs.unlinkSync(curPath);
			}
		});
		fs.rmdirSync(path);
	}
};
