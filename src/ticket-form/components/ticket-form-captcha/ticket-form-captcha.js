import template from "./ticket-form-captcha.html";
import {Component} from "main/decorators";
import "./styles.scss";

import refreshIcon from "images/icons/ic_cached_black_18px.svg";
import rightArrowIcon from "images/icons/ic_arrow_forward_black_18px.svg";

@Component({
	template,
	selector: "ticketFormCaptcha",
	bindings: {
		onChange: "&"
	}
})
export class TicketFormCaptchaComponent {
	/**
	 * @ngInject
	 */
	constructor(Captcha, $rootScope) {
		this.services = {Captcha, $rootScope};
		this.icons = {refreshIcon, rightArrowIcon};
		this.captcha = {};
	}

	$onInit() {
		const {$rootScope} = this.services;

		this.loadCaptcha();

		$rootScope.$on("ticket-form:clear-captcha", (ev) => {
			this.captcha.captchaText = "";
			this.loadCaptcha();
		});
	}

	onChangeCaptcha(value) {
		this.captcha.captchaText = value;
		this.onChange({captcha: this.captcha});
	}

	loadCaptcha() {
		const {Captcha} = this.services;

		return Captcha.getCaptcha()
			.then((data) => {
				this.captcha = data;
			});
	}
}
