
// форма подачи обращения
import {TicketFormComponent} from "./ticket-form/ticket-form.component.js";
import {TicketSuccessfulDialogComponent} from "./ticket-form/components/ticket-successeful-dialog/ticket-successeful-dialog.component.js";
import {TicketFormSystemComponent} from "./ticket-form/components/ticket-form-system/ticket-form-system.component.js";
import {TicketFormCategoryComponent} from "./ticket-form/components/ticket-form-category/ticket-form-category.component.js";
import {TicketFormLoginComponent} from "./ticket-form/components/ticket-form-login/ticket-form-login.component.js";
import {TicketFormSchoolsComponent} from "./ticket-form/components/ticket-form-schools/ticket-form-schools.component.js";

import {TicketFormDataProcessingAgreementComponent} from "./ticket-form/components/ticket-form-data-processing-agreement/ticket-form-data-processing-agreement.component.js";
import {TicketFormAgreementFormComponent} from "./ticket-form/components/ticket-form-agreement-form/ticket-form-agreement-form.component.js";
import {TicketFormCaptchaComponent} from "./ticket-form/components/ticket-form-captcha/ticket-form-captcha.js";
import {TicketFormMessageComponent} from "./ticket-form/components/ticket-form-message/ticket-form-message.component.js";

import {injectConstants} from "mesh-constants";

const meshTicketForm = angular
    .module("mesch.ticket-form", ["mesch.common", "mesch.backend"])

    .component(TicketFormComponent.selector, TicketFormComponent)
    .component(TicketSuccessfulDialogComponent.selector, TicketSuccessfulDialogComponent)
    .component(TicketFormCategoryComponent.selector, TicketFormCategoryComponent)
    .component(TicketFormSystemComponent.selector, TicketFormSystemComponent)
    .component(TicketFormLoginComponent.selector, TicketFormLoginComponent)
    .component(TicketFormSchoolsComponent.selector, TicketFormSchoolsComponent)
    .component(TicketFormDataProcessingAgreementComponent.selector, TicketFormDataProcessingAgreementComponent)
    .component(TicketFormAgreementFormComponent.selector, TicketFormAgreementFormComponent)
    .component(TicketFormCaptchaComponent.selector, TicketFormCaptchaComponent)
    .component(TicketFormMessageComponent.selector, TicketFormMessageComponent);

    injectConstants(meshTicketForm);

    export default meshTicketForm;