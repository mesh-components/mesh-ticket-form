import _ from "main/app.mixins";
import template from "./ticket-form-category.html";
import {Component} from "main/decorators";

@Component({
	template,
	selector: "ticketFormCategory",
	bindings: {
		isDisabled: "<",
		categories: "<",
		ticket: "<",
		onChange: "&"
	}
})
export class TicketFormCategoryComponent {
	/**
	 * @ngInject
	 */
	constructor() {
		this.selected = null;
		this.filteredCategories = [];
		this.options = {
			getLabel: (option) => option.title
		};
	}

	$onChanges() {
		this.$setCategories();
		this.$setSelected();
	}

	$setSelected() {
		const categoryId = _.get(this.ticket, "categoryId", null);

		this.selected = _.find(this.filteredCategories, {id: categoryId});
	}

	/**
	 * Фильтрация категорий по выбранной системе
	 */
	$setCategories() {
		const ticketExternalSystemId = _.get(this.ticket, "externalSystemId", null) || null;
		this.filteredCategories = _.filter(this.categories, {systemId: ticketExternalSystemId});
		// this.filteredCategories = _.filter(this.categories, (category) => {
		// 	if (Boolean(ticketExternalSystemId)) {
		// 		return category.systemId === ticketExternalSystemId;
		// 	}
		//
		// 	return _.isNull(category.systemId);
		// });
	}

	/**
	 * Колбэк на выбор категории обращения
	 *
	 * @param model {*} категория обращения
	 */
	changeCategory(model) {
		this.selected = model;
		this.onChange({
			category: model
		});
	}
}