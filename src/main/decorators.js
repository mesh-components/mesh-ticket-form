/**
 * Декоратор для компонента
 * @param options
 * @returns {Function}
 * @constructor
 */
export function Component(options) {
    return function (target) {
        return {...options, controller: target};
    };
}

/**
 * Разрешает вызвать метод только единожды для экземпляра класса
 * */
export function PreventTwice(target, methodName, descriptor) {
    const sourceMethod = descriptor.value;

    if (_.isFunction(sourceMethod)) {
        descriptor.value = function (...args) {
            const checkFieldName = methodName + "CallCount";
            let callCount = _.get(this, checkFieldName, 0);
            if (!callCount) {
                sourceMethod.call(this, ...args);
            } else {
                console.warn(`${methodName} is not possible to call twice. Current call count: ${callCount}`);
            }

            _.set(this, checkFieldName, callCount + 1);
        };
    } else {
        console.error(methodName + " is not a function.");
    }

    return descriptor;
}