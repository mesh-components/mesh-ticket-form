import _ from "main/app.mixins";
import template from "./ticket-form-system.html";
import {Component} from "main/decorators";

@Component({
	template,
	selector: "ticketFormSystem",
	bindings: {
		systems: "<",
		ticket: "<",
		onChange: "&"
	}
})
export class TicketFormSystemComponent {
	/**
	 * @ngInject
	 */
	constructor() {
		this.selected = null;
		this.options = {
			getLabel: (option) => `${option.title} ${option.description}`,
			getKey: (option) => option.id
		};
	}

	$onChanges() {
		this.$setSelected();
	}

	$setSelected() {
		const systemId = _.get(this.ticket, "externalSystemId", null);

		this.selected = _.find(this.systems, {id: systemId});
	}

	/**
	 * Колбэк на выбор системы
	 *
	 * @param model {*} ExternalSystems
	 */
	changeSystem(model) {
		this.selected = model;

		this.onChange({
			externalSystem: model
		});
	}
}