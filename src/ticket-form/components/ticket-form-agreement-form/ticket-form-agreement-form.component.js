import "./styles.scss";
import template from "./ticket-form-agreement-form.html";
import {Component} from "main/decorators";

import closeIcon from "images/icons/ic_clear_black_24px.svg";
import headerAgreementIcon from "images/icons/personal_data_icon.svg";

@Component({
	template,
	selector: "ticketFormAgreementForm",
	bindings: {
		close: "&"
	}
})
export class TicketFormAgreementFormComponent {
	/**
	 * @ngInject
	 */
	constructor($scope) {
		this.services = {$scope};
		this.icons = {closeIcon, headerAgreementIcon};
	}

	closeAgreementForm(){
		const {$scope} = this.services;
		$scope.$emit("support-form:view-mode-changed", "form");
	}
}