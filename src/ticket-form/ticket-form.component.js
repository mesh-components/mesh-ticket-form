import "./styles.scss";
import _ from "main/app.mixins";
import template from "./ticket-form.html";
import {Component} from "main/decorators";

import headerIcon from "images/icons/ic_chat_black_24px.svg";
import closeIcon from "images/icons/ic_clear_black_24px.svg";
import warningIcon from "images/icons/ic_warning_black_24px.svg";
import likeIcon from "images/icons/like.svg";
import sunIcon from "images/icons/sun.svg";
import requestIcon from "images/icons/request.svg";

@Component({
    template,
    selector: "ticketForm",
    bindings: {
        onClose: "&",
        close: "&",
        resolve: "<",
        isContentOnlyMode: "<" // флаг скрытия шапки и тени для впиливания формы в другие компоненты (support-form)
    }
})
export class TicketFormComponent {
    /**
     * @ngInject
     */
    constructor($rootScope, $location, $q, ExternalSystems, MeschSessions, Tickets, UrsAlert, $scope, Users, Captcha, Attachments, $uibModal, $timeout) {
        this.services = {
            $rootScope,
            $location,
            $q,
            ExternalSystems,
            MeschSessions,
            Tickets,
            UrsAlert,
            $scope,
            Users,
            Captcha,
            Attachments,
            $uibModal, $timeout
        };
        this.icons = {headerIcon, closeIcon, warningIcon, likeIcon, sunIcon, requestIcon};
        this.promise = null;
        this.ticket = null;
        this.filesToUpload = [];
        this.captcha = null;
        this.categories = [];
        this.availableCategories = [];
        this.externalSystems = [];
        this.mode = "form";
        this.formTitle = "";
        this.isDeveloper = false;

        this.withTabs = this.resolve.withTabs;
        this.tabs = [
            {title: "Написать обращение", name: "ticket", icon: requestIcon},
            {title: "Поблагодарить", name: "thanks", icon: likeIcon},
            {title: "Предложить идею", name: "idea", icon: sunIcon}
        ];
        this.selectedTab = (!this.withTabs) ? this.tabs[0] : this.resolve.isSelected ? this.tabs[0] : null;
    }

    async $onInit() {
        const {$rootScope, $q, Tickets, ExternalSystems, Users, UrsAlert} = this.services;
        //
        this.referer = _.get(this.resolve, "from", null);
        // устанавливаем заголовок формы
        this.$setFormTitle();
        // загружаем необходимые данные
        try {
            const {user, categories, externalSystems} = await (this.promise = $q.all({
                user: this.isAuthorized() ? Users.getCurrentUser() : null,
                categories: Tickets.getCategories(),
                externalSystems: ExternalSystems.getAvailableSystems()
            }));

            this.user = user;
            // this.categories = response.categories; // категории

            if (!this.isAuthorized()) {
                this.categories = _.filter(categories, (item) => [1, 2, 11, 14].indexOf(item.id) !== -1); // категории
            } else {
                this.categories = _.reject(categories, {id: 0}); // категории
            }

            this.availableCategories = _.reject(this.categories, {title: "Предложения"});
            this.externalSystems = externalSystems; // системы
        } catch (err) {
            UrsAlert.fatal();
        }

        // загружаем шаблон обращения
        await this.$loadTemplate();

        this.$setDefaultExternalSystem();

        $rootScope.$on("support-form:view-mode-changed", (ev, mode) => {
            this.mode = mode;
        });
    }

    $setDefaultExternalSystem() {
        switch (this.referer) {
            case "uchebnik":
                this.ticket.externalSystemId = 2;
                if (!this.isAuthorized()) {
                    const category = _.find(this.categories, {id: 11});
                    this.onChangeCategory(category);
                }
                break;

            case "dnevnik":
                this.ticket.externalSystemId = 1;
                if (!this.isAuthorized()) {
                    const category = _.find(this.categories, {id: 2});
                    this.onChangeCategory(category);
                }
                break;

            default:
                break;
        }
    }

    /**
     * Установка заголовка формы
     */
    $setFormTitle() {
        this.formTitle = this.isAuthorized() ?
            "Обращение в службу поддержки" :
            "Обращение в службу поддержки в случае проблемы с доступом";
    }

    /**
     * Загрузка шаблона обращения
     */
    async $loadTemplate() {
        const {Tickets, UrsAlert} = this.services;
        try {
            this.ticket = await Tickets.getTemplate();
        } catch (err) {
            UrsAlert.handled("Не удалось загрузить шаблон обращения");
        }
    }

    /**
     * Колбэк на выбор системы
     *
     * @param externalSystem {*} внешняя система
     */
    onChangeExternalSystem(externalSystem) {
        const externalSystemId = _.get(externalSystem, "id", null);
        const ticketData = {
            externalSystemId,
            problemType: "",
            categoryId: null
        };

        if (!this.isAuthorized()) {
            const titleFilter =
                (this.selectedTab.name === "ticket")
                    ? "Доступ"
                    : "Предложения";

            const category = _.find(this.categories, {systemId: externalSystemId || null, title: titleFilter});

            ticketData.categoryId = _.get(category, "id", null);
            ticketData.problemType = _.get(category, "systemTitle", "");
        }

        this.isDeveloper = false;
        this.ticket = _.assign({}, this.ticket, ticketData);
    }

    getFakeCategoryTitle() {
        const category = _.find(this.categories, {id: this.ticket.categoryId});

        return _.get(category, "title", "");
    }

    /**
     * Колбэк на выбор категории
     *
     * @param category {*} категория обращения
     */
    onChangeCategory(category) {
        this.ticket.categoryId = _.get(category, "id", null);
        this.ticket.problemType = _.get(category, "systemTitle", "");
    }

    /**
     * Колбэк на выбор школы
     *
     * @param organizationId {Number} id школы
     */
    onChangeSchool(organizationId) {
        this.ticket.organizationId = organizationId;
    }

    onChangePhone(phone) {
        if (phone && phone.substr(0, 2) !== "+7") {
            this.ticket.phone = `+7${phone}`;
        } else {
            this.ticket.phone = phone;
        }
    }

    onChangeAgreement(agreementData) {
        this.agree = agreementData.isAgree;
        // this.contactAgree = agreementData.contactAgree;
        this.contactAgree = true;
    }

    onChangeCaptcha(captcha) {
        this.captcha = captcha;
    }

    onSelectTab(tab) {
        this.selectedTab = tab;
        this.onChangeExternalSystem();
    }

    /**
     * Переход к форме авторизации
     */
    openLoginForm() {
        const {$location} = this.services;

        this.closeModal();
        $location.path("/").hash("auth");
    }

    /**
     * Проверка авторизован пользователь
     */
    isAuthorized() {
        const {MeschSessions} = this.services;

        return MeschSessions.isAuthorized();
    }

    closeModal() {
        this.close();
        this.onClose();
    }

    /**
     * Создание обращения
     */
    async createTicket(ticketForm) {
        const {$scope, Users, Tickets, UrsAlert, $rootScope} = this.services;

        $scope.$broadcast("urs-input-container:validate");

        if (!this.isAuthorized() && !this.$validateEmail(this.ticket.email)) {
            return;
        }

        if (ticketForm.$invalid) {
            return;
        }

        if (this.selectedTab.name === "idea") {
            this.$setIdeaCategory();
        }

        if (this.selectedTab.name === "thanks") {
            this.ticket.problemType = "Поблагодарить";
        }

        const userFullName = this.isAuthorized()
            ? Users.getFullUserName()
            : `${this.ticket.firstName} ${this.ticket.lastName}`;

        try {
            const attachments = await (this.promise = this.$uploadAttachments());

            this.ticket.attachments = _.chain(attachments)
                .flatten()
                .map("data[0].id")
                .value();

            if (this.isAuthorized()) {
                await (this.promise = Tickets.createTicket(this.ticket));
            } else {
                await (this.promise = Tickets.createTicket(this.ticket, this.captcha));
            }

            this.close();
            this.$openTicketSuccessfulDialog();
        } catch (err) {
            if (this.isAuthorized()) {
                UrsAlert.error(`${userFullName}, не удалось отправить ваше обращение.`, true);
            } else {
                UrsAlert.error(`Не удалось отправить ваше обращение.`, true);
            }
            $rootScope.$broadcast("ticket-form:clear-captcha");
            this.captchaText = "";
        }
    }

    $setIdeaCategory() {
        const ideaCategory = _.find(this.categories, {
            systemId: this.ticket.externalSystemId,
            title: "Предложения"
        });
        _.assign(this.ticket, {
            categoryId: ideaCategory.id,
            problemType: ideaCategory.systemTitle
        });
    }

    $validateEmail(email) {
        const re = /^(([^<>()\[\]\\.,;:\s@"!#%*=+{}'/|]+(\.[^<>()\[\]\\.,;:\s@"!#%*=+{}'/|]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Zа-яА-ЯёЁйЙщЩЫы\-0-9]+\.)+[a-zA-ZрфРФ]{2,4}))$/;

        // если email не введен, то проверка должна быть осуществлена по ngRequired

        return !_.isEmpty(email) ? re.test(email) : true;
    }

    toggleDeveloperFlag() {
        this.isDeveloper = !this.isDeveloper;
        this.ticket.organizationId = null;
        this.ticket.organizationName = "";
    }

    $uploadAttachments() {
        const {$q, Attachments} = this.services;

        const filesToUpload = _.get(this, "filesToUpload", []);

        if (_.isEmpty(filesToUpload)) {
            return $q((resolve) => {
                resolve([]);
            });
        }

        return $q.all(_.map(this.filesToUpload, (file) => {
            return Attachments.upload(file);
        }));
    }

    $openTicketSuccessfulDialog() {
        const {$uibModal} = this.services;
        $uibModal.open({
            animation: true,
            component: "ticketSuccessfulDialog",
            windowClass: "ticket-successful-dialog-modal bootstrap-iso",
            size: "md",
            resolve: {
                withTabs: () => true,
                userName: () => this.ticket.firstName
            }
        });
    }
}