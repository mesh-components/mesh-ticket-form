import "./styles.scss";
import template from "./ticket-form-login.html";
import {Component} from "main/decorators";

@Component({
	template,
	selector: "ticketFormLogin",
	bindings: {
		ticket: "<"
	}
})
export class TicketFormLoginComponent {
	/**
	 * @ngInject
	 */
	constructor() {
		this.isForget = false; // флаг "забыл(а) логин"
	}

	toggleForget(value) {
		this.isForget = !this.isForget;

		if (this.isForget) {
			this.ticket.login = "";
		}
	}
}